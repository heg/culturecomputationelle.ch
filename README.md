**La culture computationnelle**

> Les ordinateurs ne pensent pas, nous pensons pour eux. La pensée computationnelle ne consiste pas à penser comme un ordinateur mais plutôt à penser avec l'ordinateur.
>
> Deux objectifs principaux pour le cours : 
> 1. Transformer les participants en acteurs du numérique.
> 2. Réduire le clivage entre les “programmeurs” et les “autres”.


**Comment ?**

Développer une vision complète de la culture computationnelle et maitriser les principes de la pensée computationnelle.

Comprendre et appliquer les notions des algorithmes, bots, APIs (Application Programming Interfaces et des–Smartcontracts (blockchain).

Savoir décrypter l’utilité et utilisation de la culture computationnelle dans différents contextes (entreprises, services publics, initiatives citoyens).

Développer son esprit critique par rapport aux données collectées, stockées et réutilisées à travers les différentes applications.

Appliquer la pensée computationnelle pour la solution d’un problème spécifique.